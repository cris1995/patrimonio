/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import cl.uai.oop.PlaceLocation;
import cl.uai.oop.TipoLugar;
import java.util.Date;

/**
 *
 * @author cristobal
 */
public final class Places implements Comparable<Places>{
    protected String name;
    protected String description;
    protected TipoLugar lugar;
    public PlaceLocation ubicacion;
    protected Date horaApertura;
    protected Date horaCierre;

    public Places(String name, String description, TipoLugar lugar, PlaceLocation ubicacion, Date horaApertura, Date horaCierre) {
        this.name = name;
        this.description = description;
        this.lugar = lugar;
        this.ubicacion = ubicacion;
        this.horaApertura = horaApertura;
        this.horaCierre = horaCierre;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLugar(TipoLugar lugar) {
        this.lugar = lugar;
    }

    public void setUbicacion(PlaceLocation ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setHoraApertura(Date horaApertura) {
        this.horaApertura = horaApertura;
    }

    public void setHoraCierre(Date horaCierre) {
        this.horaCierre = horaCierre;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public TipoLugar getLugar() {
        return lugar;
    }

    public Date getHoraApertura() {
        return horaApertura;
    }

    public Date getHoraCierre() {
        return horaCierre;
    }

    @Override
    public int compareTo(Places o) {
       return this.ubicacion.compareTo(o.ubicacion);}


    }

    