/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.client;

import cl.uai.oop.PlaceLocation;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main {

    public static void main(String[] args) {
        try {
            String hostName = args[0];
            int portNumber = Integer.parseInt(args[1]);
            Socket mySocket = new Socket(hostName, portNumber);
            InputStream is = mySocket.getInputStream();
            OutputStream os = mySocket.getOutputStream();
            Scanner myScanner = new Scanner(System.in);
            int option = -1;
            while(true) {
                System.out.println("Bienvenido. Ingrese la opción deseada.");
                System.out.println("1 para listar los lugares cercanos");
                System.out.println("2 para encontrar información de un lugar");
                System.out.println("0 para salir");
                option = myScanner.nextInt();
                if (option == 0)
                    break;
                else if (option == 1) {
                    PlaceLocation pl = PlaceLocation.currentLocation();
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(String.format("1;%s", pl.toString()));//aqui le entrego mi ubicacion
                    dos.flush();
                    //tengo que leer cuantos espacios hay en la lista que se genera con los lugares que tengo
                DataInputStream in = new DataInputStream(is);
                //dis para recibir los datos del server
                for(int i=0;i<100;i++){
                    System.out.println(in.readUTF());
                }
                } else if (option == 2) {
                DataOutputStream dos = new DataOutputStream(os);
                System.out.println("Ingrese descripcion en una palabra");
                String descripcion=myScanner.nextLine();
                dos.writeUTF(descripcion);
                } else {
                    System.out.println("Opción inválida");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GeoIp2Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}