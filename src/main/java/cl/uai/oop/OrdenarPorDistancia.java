/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import cl.uai.oop.server.Places;
import java.util.Comparator;

/**
 *
 * @author cristobal
 */
public class OrdenarPorDistancia implements Comparator<Places>{
    PlaceLocation source;
    PlaceLocation destination;


    public OrdenarPorDistancia(PlaceLocation source) {
this.source=source;       
    }

    public int compare(Places o1, Places o2) {
        float distancia1=(float) Utils.distance(source,o1.ubicacion);
        float distancia2=(float) Utils.distance(source, o2.ubicacion);
        if(distancia1 < distancia2)
        return 1;
    else if(distancia1 > distancia2)
        return -1;
    else
    return 0;
    }
}