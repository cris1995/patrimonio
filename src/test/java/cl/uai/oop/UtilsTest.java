/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gohucan
 */
public class UtilsTest {
    
    private Faker faker = new Faker();
    private List<PlaceLocation> places = new ArrayList<>(100);
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        //aqui se crean los 100 lugares para probar en el testDistance
    for (int i = 0; i < 100; i++) {
        double lon = Double.parseDouble(faker.address().longitude().replace(',', '.'));
        double lat = Double.parseDouble(faker.address().latitude().replace(',', '.'));
        PlaceLocation lugar = new PlaceLocation(lat, lon); //instancia de PlaceLocation para crear la lista de 100 lugares
        places.add(lugar);

        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of distance method, of class Utils.
     */
    @Test
    public void testDistance(ArrayList<PlaceLocation> places) {
        double longitud = Double.parseDouble(faker.address().longitude().replace(',', '.'));
        double latitud = Double.parseDouble(faker.address().latitude().replace(',', '.'));
        PlaceLocation lugar = new PlaceLocation(latitud, longitud);
        ArrayList<Double> distancia = new ArrayList<>(100);
        for (PlaceLocation place : places) {
            Assert.assertTrue("EL lugar es coincidente",lugar.latitud- place.latitud != 0);
	    Assert.assertTrue("EL lugar es coincidente",lugar.longitud- place.longitud != 0);
/* verifica que no sea el mismo punto, si es el mismo lanza "El lugar es coincidente"*/
            distancia.add(Utils.distance(lugar, place));

        }

    }
    
}